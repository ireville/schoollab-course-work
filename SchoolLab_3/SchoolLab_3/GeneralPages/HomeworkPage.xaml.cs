﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SchoolLab_3
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomeworkPage : ContentPage
    {
        public HomeworkPage()
        {
            InitializeComponent();

            BackPhoto.Source = ImageSource.FromResource("SchoolLab_3.Images.background1.jpg");
        }

        public async void Russian_Click(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Homework_RussianPage());
        }

        public async void Literature_Click(object sender, EventArgs e)
        {
            
        }
    }
}