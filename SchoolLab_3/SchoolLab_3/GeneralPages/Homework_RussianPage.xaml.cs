﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SchoolLab_3
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Homework_RussianPage : ContentPage
    {
        public Homework_RussianPage()
        {
            InitializeComponent();
            BindingContext = new EntryMVVM();

            BackPhoto.Source = ImageSource.FromResource("SchoolLab_3.Images.hwbackground.jpg");
        }
    }
}