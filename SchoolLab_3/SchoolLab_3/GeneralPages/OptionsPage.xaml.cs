﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SchoolLab_3
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OptionsPage : ContentPage
    {
        public OptionsPage()
        {
            InitializeComponent();
            OptionsPhoto.Source = ImageSource.FromResource("SchoolLab_3.Images.options.jpg");
        }
    }
}