﻿using Xamarin.Forms;

namespace SchoolLab_3
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new MainPage();
        }
    }
}
