﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Library;
using Xamarin.Forms;

namespace SchoolLab_3
{
    public class PickerMVVMViewModel : INotifyPropertyChanged
    {
        private static int rowCnt = 15, clmCnt = 15;
        private static int rowstart = 1, clmstart = 4;

        public List<Mark> MarksList { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            // PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        // Выбранная оценка.
        private Mark selectedMark;
        public Mark SelectedMark
        {
            get { return selectedMark; }

            set {
                if (selectedMark != value)
                {
                    selectedMark = value;
                    MyMark = selectedMark.Value;
                    newValue = selectedMark.Value;
                }
            }
        }

        private static string newValue = "";

        // Текущая оценка.
        private string myMark = "-";
        public string MyMark
        {
            get {
                myMark = M;
                return myMark;
            }

            set {
                if (myMark != value)
                {
                    myMark = value;
                    // OnPropertyChanged();
                }
            }
        }

        // Заполнение поле имеющимися оценками.
        private static int i = rowstart, j = clmstart;
        public static string M
        {
            get {
                if (j == clmCnt)
                {
                    i++; j = clmstart;
                }
                if (i >= rowCnt)
                {
                    i = rowstart;
                    j = clmstart;
                }

                return General.m[i, j++] + "";
            }
        }


        /// <summary>
        /// Получает адрес ячейки и меняет для неё значение.
        /// </summary>
        /// <param name="pick"> Ячейка. </param>
        public void GetAndChangeNowPicker(Picker pick)
        {
            string pickName = pick.AutomationId;
            int[] indexes = ParsePickerName(pickName);

            General.m[indexes[0], indexes[1]] = newValue;
            General.Eight_B_Repo.Update(indexes[0], indexes[1], newValue);
        }

        /// <summary>
        /// Получает адрес ячейки по её имени.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public int[] ParsePickerName(string name)
        {
            string[] words = name.Split('_');
            int[] nums = new int[2]
                {
                    int.Parse(words[1]),
                    int.Parse(words[2])
                };
            return nums;
        }

        public PickerMVVMViewModel()
        {
            MarksList = GetMark();
            i = rowstart; j = clmstart;
        }

        /// <summary>
        /// Список оценок для выбора.
        /// </summary>
        public List<Mark> GetMark()
        {
            var marks = new List<Mark>()
            {
                new Mark(){Key = 1, Value = "1"},
                new Mark(){Key = 2, Value = "2"},
                new Mark(){Key = 3, Value = "3"},
                new Mark(){Key = 4, Value = "4"},
                new Mark(){Key = 5, Value = "5"},
                new Mark(){Key = 6, Value = "н"},
            };

            return marks;
        }
    }

    /// <summary>
    /// Оценка: id и значение.
    /// </summary>
    public class Mark
    {
        public int Key { get; set; }
        public string Value { get; set; }
    }
}