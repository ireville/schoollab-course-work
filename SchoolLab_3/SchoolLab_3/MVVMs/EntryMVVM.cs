﻿using Library;

namespace SchoolLab_3
{
    public class EntryMVVM
    {
        public string EightA_Hom
        {
            get => General.EightA_Homework;
            set {
                General.EightA_Homework = value;
            }
        }
        public string EightB_Hom
        {
            get => General.EightB_Homework;
            set {
                General.EightB_Homework = value;
            }
        }
    }
}
