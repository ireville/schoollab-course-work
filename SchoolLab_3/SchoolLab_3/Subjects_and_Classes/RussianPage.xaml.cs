﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SchoolLab_3
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RussianPage : ContentPage
    {
        public RussianPage()
        {
            InitializeComponent();

            BackPhoto.Source = ImageSource.FromResource("SchoolLab_3.Images.background1.jpg");
        }

        public async void Eight_A_Click(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Eight_APage());
        }

        public async void Eight_B_Click(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Eight_BPage());
        }
    }
}