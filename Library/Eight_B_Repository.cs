﻿using System;
using SQLite;

namespace Library
{
    public class Eight_B_Repository
    {
        SQLiteConnection connection;
        public Eight_B_Repository(string dbPath)
        {
            connection = new SQLiteConnection(dbPath);
            var createStudentTableCommand =
                   @"CREATE TABLE IF NOT EXISTS eightbtable
                   (
                     id                             integer primary key AUTOINCREMENT,
                     I                              integer,
                     J                              integer,
                     Mark                           varchar(100)
                   )";
            connection.Execute(createStudentTableCommand);
        }

        public void Insert(int newi, int newj, string newmark)
        {
            try
            {
                connection.Execute(
                    @"INSERT INTO eightbtable
                        ( I, J, Mark ) VALUES 
                        ( @i, @j, @mark );",
                    new { i = newi, j = newj, mark = newmark });
            }
            catch (Exception)
            {
            }
        }

        public void Update(int myi, int myj, string newmark)
        {
            try
            {
                connection.Execute(
                    @"UPDATE eightbtable
                        SET Mark = @mark
                        WHERE I = @i AND J = @j;",
                    new { I = myi, J = myj, mark = newmark  });
            }
            catch (Exception)
            {
            }
        }

        public string GetMark(int myi, int myj)
        {
            try
            {
                return connection.Query<Marks>(
                    @"SELECT 
                        I as I,
                        J as J,
                        mark as Mark
                        FROM eightbtable
                        WHERE I = @i AND J = @j",
                        new { i = myi, j = myj}).ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        class Marks
        { 
            public int I { get; set; }

            public int J { get; set; }

            public string Mark { get; set; }

            public Marks(int i, int j, string mark)
            {
                I = i; J = j; Mark = mark;
            }

            public Marks() { }

            public override string ToString()
                => $"{Mark}";
        }
    }
}
