﻿
namespace Library
{
    public class Picker_Item
    {
        public string Mark { get; set; }

        public Picker_Item(string mark)
        {
            Mark = mark;
        }
    }
}
