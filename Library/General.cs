﻿using System;
using System.Collections.Generic;
using Xamarin.Essentials;

namespace Library
{
    public static class General
    {
        private static int rowCnt = 15, clmCnt = 15;
        private static int rowstart = 1, clmstart = 4;

        public static string mydbPath => GetLocalFilePath("eightbtable.db");
        public static Eight_B_Repository Eight_B_Repo = new Eight_B_Repository(mydbPath);

        public static string hwdbPath => GetLocalFilePath("hwtable.db");
        public static Homework_Repository HW_Repo = new Homework_Repository(hwdbPath);

        static Random rnd = new Random();

        public static List<Picker_Item> picker_marks = new List<Picker_Item>()
        {
            new Picker_Item("н"),
            new Picker_Item("5"),
            new Picker_Item("4"),
            new Picker_Item("3"),
            new Picker_Item("2"),
            new Picker_Item("1")
        };

        private static string[] rndmarks = { "н", "1", "2", "3", "4" };

        public static string[,] m = null;

        static string default_hw_a = "Прочитать 3 главу \"Капитанской дочки\". Письменно ответить на вопросы на стр.100 " +
            "учебника.";
        static string default_hw_b = "Прочитать 4 главу \"Капитанской дочки\". Подготовиться к устному опросу.";

        public static string EightA_Homework { get; set; } = default_hw_a;
        public static string EightB_Homework { get; set; } = default_hw_b;

        /// <summary>
        /// Заполняет ячейки рандомными оценками и вносит данные в бд.
        /// </summary>
        public static void FillM()
        {
            bool isFirstEntry = false;
            if (m == null)
            {
                m = new string[rowCnt, clmCnt];
                isFirstEntry = true;
            }

            for (int i = rowstart; i < rowCnt; i++)
            {
                for (int j = clmstart; j < clmCnt; j++)
                {
                    if (isFirstEntry)
                    {
                        if (j >= clmCnt - 3)
                        {
                            m[i, j] = "-";
                            Eight_B_Repo.Insert(i, j, "-");
                        }
                        else
                        {
                            m[i, j] = rndmarks[rnd.Next(0, rndmarks.Length)];
                            Eight_B_Repo.Insert(i, j, m[i, j]);
                        }
                    }
                }
            }
        }

        public static void HW_Inserted()
        {
            if (EightA_Homework == null)
            {
                HW_Repo.Insert(1, default_hw_a);
                EightA_Homework = HW_Repo.GetHomework(1);
            }

            if (EightB_Homework == null)
            {
                HW_Repo.Insert(1, default_hw_b);
                EightB_Homework = HW_Repo.GetHomework(2);
            }
                
            if(EightA_Homework == "" && EightB_Homework == "")
            {
                HW_Repo.Update(1, "");
                HW_Repo.Update(2, "");
            }
        }

        public static string GetLocalFilePath(string filename)
        {
            return System.IO.Path.Combine(FileSystem.AppDataDirectory, filename);
        }
    }
}
