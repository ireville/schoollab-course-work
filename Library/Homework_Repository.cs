﻿using System;
using SQLite;

namespace Library
{
    public class Homework_Repository
    {
        SQLiteConnection connection;
        public Homework_Repository(string dbPath)
        {
            connection = new SQLiteConnection(dbPath);
            var createStudentTableCommand =
                   @"CREATE TABLE IF NOT EXISTS hwtable
                   (
                     id                             integer primary key AUTOINCREMENT,
                     hw                             varchar(1000)
                   )";
            connection.Execute(createStudentTableCommand);
        }

        public void Insert(int id, string homework)
        {
            try
            {
                connection.Execute(
                    @"INSERT INTO hwbtable
                        ( id, hw ) VALUES 
                        ( @myid, @homew );",
                    new { myid = id, homew = homework });
            }
            catch (Exception)
            {
            }
        }

        public void Update(int myclass, string homework)
        {
            try
            {
                connection.Execute(
                    @"UPDATE hwbtable
                        SET hw = @homew
                        WHERE id = @Id",
                    new { Id = myclass, homew = homework });
            }
            catch (Exception)
            {
            }
        }

        public string GetHomework(int myclass)
        {
            try
            {
                return connection.Query<Hws>(
                    @"SELECT 
                        id as Id,
                        hw as Hw
                        FROM hwtable
                        WHERE Id = @id AND J = @j",
                        new { id = myclass}).ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        class Hws
        { 
            public string Hw { get; set; }

            public Hws(string hw)
            {
                Hw = hw;
            }

            public Hws() { }

            public override string ToString()
                => $"{Hw}";
        }
    }
}
